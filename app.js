// koa imports
const Koa = require('koa');
const KoaRouter = require('koa-router');
const bodyParser = require('koa-bodyparser');
const cors = require('@koa/cors');

const app = new Koa();
const router = new KoaRouter();
app.use(bodyParser());
app.use(cors());

// nodemailer imports
const nodemailer = require('nodemailer');
const {google} = require('googleapis')
require('dotenv').config();

router
  .get('/', (ctx, next) => {
    ctx.body = 'Hello World!';
  })
  .post('/sendEmail', (ctx, next) => {
    console.log(ctx.request.body)
    sendMAil(ctx.request.body).then(result=>console.log('email sent...', result))
    .catch(error => console.log(error.message))
    ctx.response.body = 'Sähköposti lähetetty onnistuneesti!';
  })

const oAuth2Client = new google.auth.OAuth2(process.env.CLIENT_ID,process.env.CLIENT_SECRET,process.env.REDIRECT_URI);
oAuth2Client.setCredentials({refresh_token: process.env.REFRESH_TOKEN});

async function sendMAil(contact){
  try{
    const accessToken = await oAuth2Client.getAccessToken()

    const transporter = nodemailer.createTransport({
      service: 'gmail',
      auth: {
        type: 'OAuth2',
        user: process.env.SENDER_EMAIL,
        clientId: process.env.CLIENT_ID,
        clientSecret: process.env.CLIENT_SECRET,
        refreshToken: process.env.REFRESH_TOKEN,
        accessToken: accessToken
    },
      tls: {
          rejectUnauthorized: false
      }
    });
    
    const mailOptions = {
      from: process.env.SENDER_EMAIL,
      to: process.env.RECEIVER_EMAIL,
      subject: `${contact.name} - Yhteydenotto pyyntö`,
      text: `Viesti: ${contact.message}\n
      Nimi: ${contact.name}\n 
      Puh: ${contact.phone}\n
      S-posti: ${contact.email}\n`
    };

    const result = await transporter.sendMail(mailOptions)
    return result;

  }catch(error){
    return error
  }
}

app.use(router.routes()).use(router.allowedMethods())
app.listen(3000, () => console.log('server started'))
